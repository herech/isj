# Autor: Jan Herec, xherec00
# Datum vytvoření: 3. 5. 2014
# Popis: - Projekt do předmetu ISJ, jeho obsahem je převod publikací do formátu BibTeXML
#        - Program získává jako argument adresu stránky, z té vytáhne seznam publikací, ty převede do formátu BibTeXML,
#          který vypíše na stdout nebo do souboru, pokud je zadán přepínač -f
# Syntaxe spuštění: python3 bibtexml.py pageUrl [-f outputFilename]

# standartní moduly
try:
    import os
    import sys
    import re
    import math
    import collections
    import urllib.request
    import hashlib

    # speciální moduly
    import html5lib
    from bs4 import BeautifulSoup
except:
    sys.stderr.write("CHYBA! Nebyl nahrán některý z vyžadovaných modulů\n")
    sys.exit(1)

def main():
    """ Klasický main, který koordinuje posloupnost akcí programu """

    # kontrola, jestli se zdařilo nahrání speciálních klíčových modulů, pokud ne, nemá cenu aby program pokračoval
    if ("html5lib" not in sys.modules or "bs4" not in sys.modules):
        sys.stderr.write("CHYBA! Nebyl nahrán některý z vyžadovaných modulů: html5lib, bs4 (BeautifulSoup)\n")
        sys.exit(1)

    # jednoduchá kontrola a načtení argumentů
    if (len(sys.argv) == 4 and "-f" == sys.argv[1]):
        url = sys.argv[3]
        print_output_to_file = True
        name_of_output_file = sys.argv[2]
    elif (len(sys.argv) == 4 and "-f" in sys.argv[2]):
        url = sys.argv[1]
        print_output_to_file = True
        name_of_output_file = sys.argv[3]
    elif (len(sys.argv) == 2):
        url = sys.argv[1]
        print_output_to_file = False
        name_of_output_file = ""
    else:
        sys.stderr.write("CHYBA! Skriptu nebyly předány argumenty ve správném formátu\n" \
                         "Syntaxe spouštení skriptu je následující: python3 bibtexml.py pageUrl [-f outputFilename]\n")
        sys.exit(1)

    # oveření, jestli se podařilo načíst stránku, kterou uživatel specifikoval v předaném argumentu,
    # pokud ne, tak nemá cenu aby program dále pokračoval
    try:
        response = urllib.request.urlopen(url)
        html = response.read()
    except:
        sys.stderr.write("CHYBA! Nepodařilo se získat data ze stránky: " + url + "\n")
        sys.exit(1)

    # stránku správně rozkódujeme a takto ji předáme objektu BeautifulSoup,
    # který ji pomocí parseru html5lib rozparsuje, takže s ní budeme moci pohodlně a efektivně pracovat
    soup = BeautifulSoup(html, "html5lib", from_encoding=response.headers.get_content_charset())

    # seznam publikací, které jsou reprezentovány řetezci bibliografických údajů
    list_of_publications = []

    # promenná indikuje, jestli je možné na dané stránce získat přímo bibliografické údaje ve formátu bibtex
    publication_in_bibtex_finded = False

    # seznam publikací ve formátu bibtex (reprezentováno řetězci)
    publication_in_bibtex = []

    # víceúrovňový slovník, kde klíče představují ID publikace (entryID) a položky představují opět slovníky,
    # kde klíče představují atributy publikace a hodnoty předtavují hodnoty těchto atributů 
    dictionary_of_publications = collections.defaultdict(dict)

    pattern_for_types_of_publications = "@article|@book|@booklet|@conference|@inbook|@incollection|@inproceedings|@manual|@mastersthesis|" \
                                        "@misc|@phdthesis|@proceedings|@techreport|@unpublished"

    # na stránce hledáme bibliografické záznamy ve formátu bibtex
    for link in soup.find_all(text=re.compile(pattern_for_types_of_publications, re.IGNORECASE)):
        publication_in_bibtex_finded = True
        publication_in_bibtex.append(re.sub("\s+" , " ", str(link.parent.get_text())))

    # pokud takové záznamy existují, tak je použijeme
    if (publication_in_bibtex_finded == True):
        list_of_publications = publication_in_bibtex
        dictionary_of_publications = process_publications(list_of_publications, True)

    # pokud takové záznamy neexistují, nedá se nic dělat a budeme muset ze stránek vyparsovat potřebná data sami
    else:
        list_of_publications = get_publications_from_page(soup)
        dictionary_of_publications = process_publications(list_of_publications, False)

    # nakonec víceúrovňový slovník publikací vytiskneme formátu BibTeXML
    print_bibtexml(dictionary_of_publications, print_output_to_file, name_of_output_file)


def get_publications_from_page(soup):
    """ Funkce získává jako parametr objekt typu BeautifulSoup,
    který reprezentuje předzpracovanou stránku se seznamem publikací.
    V této stránce hledá záznamy publikací (v elementech TD, LI a P),
    které následně vrátí jako seznam řetězců. """

    list_of_publications = []

    # počet TD elementů na stránce
    number_of_TD_elements = 0
    # celkový počet řetezců, které obsahují TD elementy
    number_of_TD_strings = 0
    # seznam publikací v elentech TD
    publication_in_TD = []

    # obdobně jako u TD elementů, viz. výše
    number_of_LI_elements = 0
    number_of_LI_strings = 0
    publication_in_LI = []

    # obdobně jako u TD elementů, viz. výše
    number_of_P_elements = 0
    number_of_P_strings = 0
    publication_in_P = []

    # průměrný počet řetězců, který obsahuje každý jeden TD element
    average_number_of_TD = 0.0
    # průměrný počet řetězců, který obsahuje každý jeden LI element
    average_number_of_LI = 0.0
    # průměrný počet řetězců, který obsahuje každý jeden P element
    average_number_of_P = 0.0

    # zde nalezneme místo, kde pravděpodobně začíná výčet publikací a nastavíme toto místo jako výchozí,
    # od kterého se bude prohledávat stránka.
    for link in soup.find_all(name=re.compile("p|font|h1|h2|h3|h4", re.IGNORECASE),text=re.compile("publications", re.IGNORECASE), limit=1):
        soup = link

    #procházíme všechny elementy TD
    for link in soup.find_all_next('td'):
        # daný element TD budeme započítávat, pokud splňuje všechny tyto podmínky:
        #     - neobsahuje další vnořené elementy stejného typu
        #     - zároveň jeho rodič obsahuje i další elemnty tohoto typu, tzn. nejedná se o jedináčka
        #     - a obsahuje alespoň 2 řetězce (tzn. že tímto odfiltrujeme prázdné elementy a elementy, jejichž obsah nemá žádnou strukturu)
        if (len(list(link.find_all('td'))) == 0 and (len(list(link.parent.find_all('td'))) > 1) and len(list(link.stripped_strings)) > 1):
            number_of_TD_elements += 1
            number_of_TD_strings += len(list(link.stripped_strings))
            # publikaci uložíme do seznamu publikací daného typu, ale předtím ji ještě zpracujeme:
            #     - odstraníme posloupnosti bílých znaků
            #     - a odstraníme metainformace k publikacím
            publication_in_TD.append(re.sub("\s+" , " ", re.sub("\[.+\]" , "", re.sub("\(details\)" , "", "".join(map(str, link.contents))))))

    #procházíme všechny elementy LI, způsob jakým je zpracováváme je totižný jako u TD elementů, viz. výše
    for link in soup.find_all_next('li'):
        if ((len(list(link.find_all('li'))) == 0) and (len(list(link.parent.find_all('li'))) > 1) and len(list(link.stripped_strings)) > 1):
            number_of_LI_elements += 1
            number_of_LI_strings += len(list(link.stripped_strings))
            publication_in_LI.append(re.sub("\s+" , " ", re.sub("\[.+\]" , "", re.sub("\(details\)" , "", "".join(map(str, link.contents))))))

    # ještě než vyselectujeme P elementy, tak odstraníme jejich rodiče, tedy je vynoříme z jedné úrovně
    # motivace je taková, že publikace mohou být obaleny jednotlivě jako: <div><p>publikace</p></div>
    # a pak by neprošly podmínkou: len(list(link.parent.find_all('p'))) > 1
    for link in soup.find_all_next('p'):
        s1 = link.parent
        if (hasattr(s1,'name') == True and s1.name == "div"):
            s1.unwrap()

    #procházíme všechny elementy LI, způsob jakým je zpracováváme je totižný jako u TD elementů, viz. výše
    for link in soup.find_all_next('p'):
        if (len(list(link.find_all('p'))) == 0 and (len(list(link.parent.find_all('p'))) > 1) and len(list(link.stripped_strings)) > 1):
            number_of_P_elements += 1
            number_of_P_strings += len(list(link.stripped_strings))
            publication_in_P.append(re.sub("\s+" , " ", re.sub("\[.+\]" , "", re.sub("\(details\)" , "", "".join(map(str, link.contents))))))

    # pokud jsme našli nejaké TD elementy, tak vypočítáme průměrný počet stringů v elementech
    if (number_of_TD_elements > 0.0):
        average_number_of_TD = math.floor(number_of_TD_strings / number_of_TD_elements)
    # obdobně jako u TD elementů, viz. výše
    if (number_of_LI_elements > 0.0):
        average_number_of_LI = math.floor(number_of_LI_strings / number_of_LI_elements)
    # obdobně jako u TD elementů, viz. výše
    if (number_of_P_elements > 0.0):
        average_number_of_P = math.floor(number_of_P_strings / number_of_P_elements)

    # Postup, který nám zajistí, že do seznamu publikací přiřadíme opravdový seznam publikací na stránce
    if (average_number_of_TD * number_of_TD_elements > average_number_of_LI * number_of_LI_elements
        and average_number_of_TD * number_of_TD_elements > average_number_of_P * number_of_P_elements):
        list_of_publications = publication_in_TD

    elif (average_number_of_LI * number_of_LI_elements > average_number_of_TD * number_of_TD_elements
          and average_number_of_LI * number_of_LI_elements > average_number_of_P * number_of_P_elements):
        list_of_publications = publication_in_LI

    elif (average_number_of_P * number_of_P_elements > average_number_of_TD * number_of_TD_elements
          and average_number_of_P * number_of_P_elements > average_number_of_LI * number_of_LI_elements):
        list_of_publications = publication_in_P

    return list_of_publications


def process_publications(list_of_publications, publications_in_bibtex):
    """ Funkce, která zpracuje seznam publikací ve formátu řetězců (první argument) a vrátí strukturovaný slovník publikací.
    Druhý argument funkce indikuje, jestli je seznam řetězců ve formátu BibTeXML """

    dictionary_of_publications = collections.defaultdict(dict)

    # pokud je seznam řetězců ve formátu BibTeXML
    if (publications_in_bibtex == True):

        # zpracujeme každý řetězec (publikaci):
        # (vykousneme z něj požadované bibliografické informace a uložíme je strukturovaně do víceúrovňového slovníku)
        for publication in list_of_publications:
            type_of_publication = re.search("^@([a-z]+){.+", publication, re.IGNORECASE).groups()[0]
            entryID = re.search("^@[a-z]+{([^,]+),.+", publication, re.IGNORECASE).groups()[0]
            items_of_publication = re.findall(", ([^ ,=]+) = {([^}]+)", publication)
            dictionary_of_publications[entryID]["type_of_publication"] = type_of_publication.lower()

            for item in items_of_publication:
                # atribut file nás nezajímá...
                if (item[0].find("file") != -1):
                    continue
                dictionary_of_publications[entryID][item[0]] = item[1]

        return dictionary_of_publications

    # pokud seznam řetězců není ve formátu BibTeXML, a tedy se jedná o nijak nestrukturovaná data
    else:

        # pořadí položky ve slovníku, díky tomu budeme moci vypisovat publikace seřazeně tak jak jsou na stránce
        order = 0
        # zpracujeme každý řetězec (publikaci):
        # (vykousneme z něj požadované bibliografické informace a uložíme je strukturovaně do víceúrovňového slovníku)
        for publication in list_of_publications:

            # tato proměnná bude dále uchovávat publikaci s s html tagy, ale bez jejich atributů
            publication_with_tags = re.sub(r'(<[^> ]+) [^>]+>', r'\1>', publication)
            publication_with_tags = re.sub(r'<span>([^>]+)</span>', r'\1', publication_with_tags)

            # tato proměnná bude představovat a uchovávat publikaci, ale bez html tagů
            publication = re.sub(r'<[^>]*?>', '', publication_with_tags)
            # pokusime se zjistit cast autoru
            castAutoru = re.search("^([^\.,]+)[\.,]", publication)
            # pokud nelze zjistit cast autoru, tak se pravdepodobne nejedna o publikaci a preskocime na zpracovani dalsi publikace
            # taky pokud je řetězec podezřele dlouhý, tak se asi nejedná o publikaci
            if (castAutoru == None or len(publication) > 1000):
                continue

            # část autorů upravíme tak, abychom je mohli použít jako část klíče publikace
            castAutoru2 = castAutoru.groups()[0].encode('ascii', 'ignore').decode("utf-8").strip().replace (" ", "-")

            # maximální délka řetězce autorů je 50 znaků
            if (len(castAutoru2) > 50):
                castAutoru2 = castAutoru2[0:50]

            # klíč publikace (skládá se z pořadí, řetězce části autorů, roku a dvoumístného hashe publikace)
            entryID = str(order) + "_" + castAutoru2 + ":" + find_year(publication) + ":" + \
                      hashlib.md5(str(publication).encode("utf-8")).hexdigest()[0:2]

            # zjistíme typ publikace 
            type_of_publication = get_type_of_publication(publication)

            # index, podle kterého budeme řadit prvky ve slovníku a budeme tak určovat pořadí výpisu
            i = 0

            # index bude prefixem pro jednotlivé typy publikací
            title = str(i + 1) + "_title"
            authors = str(i) + "_author"
            month = str(i + 5) + "_month"
            year = str(i + 6) + "_year"
            adress = str(i + 4) + "_address"
            journal = str(i + 2) + "_journal"
            volume = str(i + 7) + "_volume"
            number = str(i + 8) + "_number"
            pages = str(i + 9) + "_pages"
            issn = str(i + 10) + "_issn"
            booktitle = str(i + 2) + "_booktitle"
            publisher = str(i + 3) + "_publisher"
            pages = str(i + 9) + "_pages"
            isbn = str(i + 10) + "_isbn"
            type = str(i + 2) + "_type"
            school = str(i + 3) + "_school"

            dictionary_of_publications[entryID] = collections.defaultdict(dict)

            # pokusíme se zjistit atributy, které jsou společné pro všechny typy publikací
            dictionary_of_publications[entryID]["type_of_publication"] = type_of_publication
            dictionary_of_publications[entryID][title] = find_title(publication_with_tags)
            dictionary_of_publications[entryID][authors] = find_authors(publication_with_tags, dictionary_of_publications[entryID][title])
            dictionary_of_publications[entryID][month] = find_month(publication)

            dictionary_of_publications[entryID][year] = find_year(publication)
            dictionary_of_publications[entryID][adress] = find_adress(publication_with_tags,
                                                                      dictionary_of_publications[entryID][authors],
                                                                      dictionary_of_publications[entryID][title],
                                                                      dictionary_of_publications[entryID][year],
                                                                      dictionary_of_publications[entryID][month])

            # pro každý typ publikace se pokusíme zjistit charakteristické infromace
            if (type_of_publication == "article"):
                dictionary_of_publications[entryID][journal] = find_journal(publication_with_tags, dictionary_of_publications[entryID][title])
                dictionary_of_publications[entryID][volume] = find_volume(publication)
                dictionary_of_publications[entryID][number] = find_number(publication)
                dictionary_of_publications[entryID][pages] = find_pages(publication)
                dictionary_of_publications[entryID][issn] = find_issn(publication)
                '''print("-----------ARTICLE------------")
                print(publication,"\n")'''
            elif (type_of_publication == "inproceedings"):
                dictionary_of_publications[entryID][booktitle] = find_booktitle(publication_with_tags, dictionary_of_publications[entryID][title])
                dictionary_of_publications[entryID][publisher] = find_publisher(publication_with_tags, dictionary_of_publications[entryID][title])
                dictionary_of_publications[entryID][pages] = find_pages(publication)
                dictionary_of_publications[entryID][isbn] = find_isbn(publication)
                '''print("--------IN PROCEEDINGS--------")
                print(publication,"\n")'''
            elif (type_of_publication == "book"):
                dictionary_of_publications[entryID][publisher] = find_publisher(publication_with_tags, dictionary_of_publications[entryID][title])
                dictionary_of_publications[entryID][isbn] = find_isbn(publication)
                '''print("-------------BOOK-------------")
                print(publication,"\n")'''
            elif (type_of_publication == "inbook"):
                dictionary_of_publications[entryID][booktitle] = find_booktitle(publication_with_tags, dictionary_of_publications[entryID][title])
                dictionary_of_publications[entryID][publisher] = find_publisher(publication_with_tags, dictionary_of_publications[entryID][title])
                dictionary_of_publications[entryID][pages] = find_pages(publication)
                dictionary_of_publications[entryID][isbn] = find_isbn(publication)
                '''print("-----------IN BOOK------------")
                print(publication,"\n")'''
            elif (type_of_publication == "phdthesis"):
                dictionary_of_publications[entryID][type] = "Ph.D Dissertation"
                dictionary_of_publications[entryID][school] = find_school(publication_with_tags, dictionary_of_publications[entryID][title])
                '''print("----------PHD THESIS----------")
                print(publication,"\n")'''
            elif (type_of_publication == "masterthesis"):
                dictionary_of_publications[entryID][type] = "Master Thesis"
                dictionary_of_publications[entryID][school] = find_school(publication_with_tags, dictionary_of_publications[entryID][title])
                '''print("--------MASTER THESIS---------")
                print(publication,"\n")'''
            elif (type_of_publication == "misc"):
                dictionary_of_publications[entryID][pages] = find_pages(publication)
                dictionary_of_publications[entryID][isbn] = find_isbn(publication)
                '''print("-------------MISC-------------")
                print(publication,"\n")'''

            order += 1

    return dictionary_of_publications


def get_type_of_publication(publication):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace a na jejich základě vrací typ publikace.
    Funkce rozlišuje 6 základních typů, pokud nesedí ani jeden, vrací typ misc. """

    #article
    if (re.search("magazine|journal|issn|issue|vol\. \d+|volume \d+|\d+\(\d+\)(: ?\d+-\d+)?", publication, re.IGNORECASE) != None
        and re.search("isbn", publication, re.IGNORECASE) == None):
        return "article"

    #inprocessing
    elif(re.search("kongress|workshop|conference|conf\.|proc\.|proceedings|([^a-z]in[^a-z].*(?!eds?\.)(pages [\d]|pp\.? [\d]|p\.? [\d]).*isbn)",
                   publication, re.IGNORECASE) != None
         or re.search("[A-Z]+ ?'[0-9]+", publication, re.IGNORECASE) != None):
        return "inproceedings"

    #book
    elif((re.search("isbn", publication, re.IGNORECASE) != None
         and re.search("pp\.? [\d]+|pages [\d]+|p\.? [\d]+", publication, re.IGNORECASE) == None
         and re.search("[^a-z]in[^a-z].*(?!eds?\.)", publication, re.IGNORECASE) == None)
         or (re.search("edition", publication, re.IGNORECASE) != None)
         or (re.search("pp\.? [\d]+|pages [\d]+|p\.? [\d]+", publication, re.IGNORECASE) == None
             and re.search("[^a-z]in[^a-z].+eds?\.", publication, re.IGNORECASE) != None)):
        return "book"

    #inbook
    elif(re.search("(?=pp\.? [\d]|pages [\d]|p\.? [\d])(?=(isbn)?)", publication, re.IGNORECASE) != None):
        return "inbook"

    #phd thesis
    elif(re.search("ph\.?d\.? thesis|ph\.?d\.? dissertation", publication, re.IGNORECASE) != None):
        return "phdthesis"

    #master thesis
    elif(re.search("(master.?s?)? thesis", publication, re.IGNORECASE) != None):
        return "masterthesis"

    #misc
    else:
        return "misc"


def find_year(publication):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace,
    v nich se pokusí najít rok vydání publikace, a tento rok následně vrací.
    Pokud se jej nepodaří nalézt, vrací prázdný řetězec. """

    year = re.findall("[^\-\d](\d{4})[^\-\d]|[^\-\d](\d{4})$", publication)
    if (year):
        if (year[-1][0]):
            return year[-1][0]
        else:
            return year[-1][1]
    else:
        return ""


def find_month(publication):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace,
    v nich se pokusí najít měsíc vydání publikace, a tento měsíc následně vrací.
    Pokud se jej nepodaří nalézt, vrací prázdný řetězec. """
    
    month_pattern = "January|February|March|April|May|June|July|August|September|" \
                    "October|November|December"
    month = re.findall(month_pattern, publication, re.IGNORECASE)

    if (month):
        return month[-1]
    else:
        return ""


def find_pages(publication):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace,
    v nich se pokusí najít rozsah stran z dané publikace, a tyto strany vrací.
    Pokud se je nepodaří nalézt, vrací prázdný řetězec. """
    
    pages = re.search("pp\.? ([\d]+(-+[\d]+)?)|pages ([\d]+(-+[\d]+)?)|p\.? ([\d]+(-+[\d]+)?)", publication, re.IGNORECASE)
    if (pages):
        if (pages.groups()[0]):
            return re.sub("(\d+)-(\d+)" , r"\1--\2", pages.groups()[0])
        elif (pages.groups()[2]):
            return re.sub("(\d+)-(\d+)" , r"\1--\2", pages.groups()[2])
        elif (pages.groups()[4]):
            return re.sub("(\d+)-(\d+)" , r"\1--\2", pages.groups()[4])
    else:
        return ""


def find_issn(publication):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace,
    v nich se pokusí najít issn publikace, a toto issn vrací.
    Pokud se jej nepodaří nalézt, vrací prázdný řetězec. """
    
    issn = re.search("issn.? (\d+(-\d+)+)", publication, re.IGNORECASE)
    if (issn):
        return issn.groups()[0]
    else:
        return ""


def find_isbn(publication):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace,
    v nich se pokusí najít isbn publikace, a toto isbn vrací.
    Pokud se jej nepodaří nalézt, vrací prázdný řetězec. """
    
    isbn = re.search("isbn.? (\d+(-\d+)+)", publication, re.IGNORECASE)
    if (isbn):
        return isbn.groups()[0]
    else:
        return ""


def find_volume(publication):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace,
    v nich se pokusí najít volume publikace a toto vrací.
    Pokud se jej nepodaří nalézt, vrací prázdný řetězec. """
    
    volume = re.findall("vol\. (\d+)|volume (\d+)|(\d+)\(\d+\)|(\d+):\d+|(\d+), \d+, \d{4}", publication, re.IGNORECASE)
    if (volume):
        if (volume[-1][0]):
            return volume[-1][0]
        elif (volume[-1][1]):
            return volume[-1][1]
        elif (volume[-1][2]):
            return volume[-1][2]
        elif (volume[-1][3]):
            return volume[-1][3]
        elif (volume[-1][4]):
            return volume[-1][4]
    else:
        return ""


def find_number(publication):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace,
    v nich se pokusí najít číslo publikace a toto vrací.
    Pokud se jej nepodaří nalézt, vrací prázdný řetězec. """
    
    number = re.findall("no\. (\d+)|issue (\d+)|number (\d+)|\d+\((\d+)\)|\d+, (\d+), \d{4}", publication, re.IGNORECASE)
    if (number):
        if (number[-1][0]):
            return number[-1][0]
        elif (number[-1][1]):
            return number[-1][1]
        elif (number[-1][2]):
            return number[-1][2]
        elif (number[-1][3]):
            return number[-1][3]
        elif (number[-1][4]):
            return number[-1][4]
    else:
        return ""


def find_authors(publication, title):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace,
    v nich se pokusí najít autory publikace a tyto autory vrací.
    Také přijímá řetězec s názvem publikace, díky němuž se může v hledání autorů lépe zorientovat.
    Pokud se je nepodaří nalézt, vrací prázdný řetězec. """
    result = ""

    # hledání realizujeme vlevo od názvu publikace
    authors = re.sub(r''+ title +'.*$', r'', publication)
    authors = re.sub(r'<[^>]+>', '', authors.strip())
    if (len(authors) > 3):
        result = authors
    # hledání realizujeme vpravo od názvu publikace
    else:
        author = re.sub(r'^.*'+ title +'', r'', publication)
        authors2 = re.search("^([^0-9]+?)[A-Z0-9]{2,}", author)
        if (authors2):
            result = authors2.groups()[0]

    # odřízeneme smetí na konci řetězce autorů
    return result.strip("\"").strip(",").strip(" ").strip(",").strip("á").strip(" ").strip(",").strip(" ").strip(".")


def find_title(publication):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace,
    v nich se pokusí najít název publikace a tento název vrací.
    Pokud se jej nepodaří nalézt, vrací prázdný řetězec. """

    result = ""

    # název publikace může být v tagu a nnásledovaného uvozovkami
    title = re.search("<a>([^>]+)</a>.*[^=]+\"[^\"`]+[\"`]+", publication, re.IGNORECASE)
    if (title):
        result = title.groups()[0]

    # název publikace může být v uvozovkách
    title1 = re.search("[^=]+\"([^\"`]+)[\"`]+", publication, re.IGNORECASE)
    if (title1):
        return title1.groups()[0]
    # název publikace může být v tagu a na libovolném místě
    else:
        title2 = re.findall("<a>([^>]+)</a>", publication, re.IGNORECASE)
        if (title2 and len(title2) == 1):
            result = title2[0]
        elif (title2 and len(title2) > 1):
            # název publikace může být v tagu b, který následuje za tagem a
            title3 = re.search("<a>[^>]+</a>.*<b>([^>]+)</b>", publication, re.IGNORECASE)
            if (title3):
                result = title3.groups()[0]
            else:
                result = title2[0]

    return result.strip("\"").strip(",").strip(" ").strip(",").strip(".").strip(",")


def find_adress(publication, authors, title, year, month):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace,
    v nich se pokusí najít adresu vydání publikace a tuto adresu vrací.
    Také přijímá řetězec s názvem publikace, díky němuž se může v hledání adresy lépe zorientovat.
    Pokud se ji nepodaří nalézt, vrací prázdný řetězec. """
    result = ""

    # hledání realizujeme vpravo od názvu publikace
    adress = re.sub(r'^.*'+ title +'', r'', publication)
    adress = re.sub(r'<[^>]+>', '', adress.strip())
    adress = re.sub(r'In:', '', adress, re.IGNORECASE)
    adress2 = re.search("([a-z ]+): [a-z ]+", adress, re.IGNORECASE)

    # místo vydání a vydavatel může být v obvyklém tvaru, např. Brno: FIT VUT
    if (adress2):
        result = adress2.groups()[0]
    # a nebo v jiném tvaru, potom jej hledáme v kontextu s rokem nebo měsícem, vedle nichž by se měl nacházet s největší pravděpodobností
    else:
        adress2 = re.search("([a-z \-]+), "+ year +"", publication, re.IGNORECASE)
        if (adress2):
            result = adress2.groups()[0]
        else:
            adress2 = re.search("([a-z \-]+), "+ month +"", publication, re.IGNORECASE)
            if (adress2):
                result = adress2.groups()[0]

    # omezíme ty úplně největší nalezené hlouposti, pořád ale neomezíme úplně všechny
    if (len(result) > 50 or (len(month) > 1 and month in result)):
        result = ""

    # často je adresa chybně vyhledána a představuje prvního autora, nebo je vyhledána jiná nějaká hloupost
    result = result.strip("\"").strip(",").strip(" ")
    if (result not in authors and len(result) > 2):
        return result
    else:
        return ""


def find_publisher(publication, title):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace,
    v nich se pokusí najít vydavatele publikace a tohoto vrací.
    Také přijímá řetězec s názvem publikace, díky němuž se může v hledání vydavatele lépe zorientovat.
    Pokud se jej nepodaří nalézt, vrací prázdný řetězec. """
    result = ""

    # hledání realizujeme vpravo od názvu publikace
    publisher = re.sub(r'^.*'+ title +'', r'', publication)
    publisher = re.sub(r'<[^>]+>', '', publisher.strip())
    publisher = re.sub(r'In:', '', publisher, re.IGNORECASE)
    publisher2 = re.search("[a-z ]+: ([a-z -]+)", publisher, re.IGNORECASE)
    if (publisher2):
        result = publisher2.groups()[0]

    return result.strip("\"").strip(",").strip(" ")


def find_journal(publication, title):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace,
    v nich se pokusí najít časopis a tento vrací.
    Také přijímá řetězec s názvem článku, díky němuž se může v hledání časopisu lépe zorientovat.
    Pokud se jej nepodaří nalézt, vrací prázdný řetězec. """
    result = ""

    # hledání realizujeme vpravo od názvu publikace
    journal = re.sub(r'^.*'+ title +'', r'', publication)
    journal2 = re.search("<i>(.*)</i>|<em>(.*)</em>", journal, re.IGNORECASE)
    # journal se nachází v em
    if (journal2 and journal2.groups()[0]):
        result = journal2.groups()[0]
    # journal se nachází v i
    elif (journal2 and journal2.groups()[1]):
        result = journal2.groups()[1]
    # journal se nachází jinde než v em, nebo i, nejspíše ale pořád vpravo od názvu publikace
    else:
        journal = re.sub(r'<[^>]+>', '', journal.strip(" ,"))
        journal2 = re.search("^,? ([^,]+),", journal, re.IGNORECASE)
        if (journal2):
            result = journal2.groups()[0]

    return result.strip("\"").strip(",").strip(" ")


def find_booktitle(publication, title):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace,
    v nich se pokusí najít název sborníku a tento vrací.
    Také přijímá řetězec s názvem článku ve sborníku, díky němuž se může v hledání sborníku lépe zorientovat.
    Pokud se jej nepodaří nalézt, vrací prázdný řetězec. """
    result = ""

    # hledání realizujeme vpravo od názvu publikace
    booktitle = re.sub(r'^.*'+ title +'', r'', publication)
    booktitle2 = re.search("<i>(.*)</i>|<em>(.*)</em>", booktitle, re.IGNORECASE)
    # booktitle se nachází v em
    if (booktitle2 and booktitle2.groups()[0]):
        result = booktitle2.groups()[0]
    # booktitle se nachází v i
    elif (booktitle2 and booktitle2.groups()[1]):
        result = booktitle2.groups()[1]
    # booktitle se nachází jinde než v em, nebo i, nejspíše ale pořád vpravo od názvu publikace
    else:
        booktitle = re.sub(r'<[^>]+>', '', booktitle.strip(" ,"))
        booktitle2 = re.search("^,? ([^,]+),", booktitle, re.IGNORECASE)
        if (booktitle2):
            result = booktitle2.groups()[0]

    return result.strip("\"").strip(",").strip(" ")


def find_school(publication, title):
    """ Funkce přijímá řetězec představující bibliografické údaje publikace,
    v nich se pokusí najít název školy a tento vrací.
    Také přijímá řetězec s názvem disertační práce, díky němuž se může v hledání školy lépe zorientovat.
    Pokud se jej nepodaří nalézt, vrací prázdný řetězec. """
    result = ""

    # hledání realizujeme vpravo od názvu publikace
    school = re.sub(r'^.*'+ title +'', r'', publication)
    school = re.sub(r'<[^>]+>', '', school.strip(" ,"))
    school2 = re.search("^,? ([^,]+),", school, re.IGNORECASE)
    if (school2):
        result = school2.groups()[0]

    return result.strip("\"").strip(",").strip(" ")
    

def print_bibtexml(dictionary_of_publications, print_output_to_file, name_of_output_file):
    """ Funkce přijímá víceúrovňový slovník publikací, dále příznak určijící,
    kam budou informace vypisovány a nakonec případné jméno výstupního souboru.
    Informace z tohoto slovníku vypisuje na stdout, nebo do souboru a to ve formátu BibTeXML """

    # pokusíme se otevřít soubor pro zápis, pokud operace selže, budeme dále vypisovat na stdout namísto do souboru
    if (print_output_to_file == True):
        try:
            file = open(name_of_output_file, mode='w', encoding='utf-8')
        except:
            sys.stderr.write("CHYBA! Soubor: " + name_of_output_file + " se nepodařilo otevřít. Data budou zapsána na stdout\n")
            print_output_to_file = False

    # do proměnné vystup postupně uložíme veškeré informace, které nakonec vypíšeme, zde vytváříme hlavičku xml souboru
    vystup = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>" + "\n"
    vystup += "<?xml-stylesheet href=\"bibtexml.css\" type=\"text/css\"?>" + "\n"
    vystup += "<!DOCTYPE bibtex:file PUBLIC \"-//BibTeXML//DTD XML for BibTeX v1.0//EN\" \"bibteXML.dtd\">" + "\n\n"
    vystup += "<bibtex:file xmlns:bibtex=\"http://bibtexml.sf.net/\">" + "\n\n"

    # postupně projdeme víceúrovňový slovník publikací a zajistíme jejich vypsání ve formátu BibTeXML
    for entryID in sorted(dictionary_of_publications.keys(), key = lambda x: x.split("_")[0]):
        vystup += "    <bibtex:entry id=\"" + re.sub("^\d+_", "", entryID) + "\">" + "\n"
        vystup += "        <bibtex:" + dictionary_of_publications[entryID]["type_of_publication"] + ">" + "\n"

        for item in sorted(dictionary_of_publications[entryID].keys(), key = lambda x: x.split("_")[0]):
            if (item == "type_of_publication" or dictionary_of_publications[entryID][item] == ""):
                continue
            else:
                vystup += "            <bibtex:" + re.sub("^\d+_", "", item) + ">" + \
                re.sub(r'<[^>]*?>', '', dictionary_of_publications[entryID][item]) + "</bibtex:" + re.sub("^\d+_", "", item) + ">" + "\n"

        vystup += "        </bibtex:" + dictionary_of_publications[entryID]["type_of_publication"] + ">" + "\n"
        vystup += "    </bibtex:entry>" + "\n\n"

    vystup += "</bibtex:file>\n"

    # pokusíme se o zápis, pokud selže, budeme vše vypisovat namísto do souboru na stdout
    if (print_output_to_file == True):
        try:
            file.write(vystup)
        except:
            sys.stderr.write("CHYBA! Do souboru: " + name_of_output_file + " se nepodařilo zapsat data. Data budou zapsána na stdout\n")
            print_output_to_file = False
            print(vystup)
    else:
        print(vystup)

    if (print_output_to_file == True):
        file.close()

    return

# zajístí nám, že budeme začínat program ve funkci main, v tuto chvíli už máme taky načteny všechny funkce
if __name__ == "__main__":
    main()
